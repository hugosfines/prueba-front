const app = {
    data() {
        return {
            user: '',
            email: '',
            password: '',
            showLogin: true
        }
    },
    methods: {
        getUsers() {
            fetch('https://969rgz78f9.execute-api.us-east-1.amazonaws.com/dev/api/user')
            .then(response => response.json())
            .then(data => {
                //console.log(data)
            });
        },
        sendFormLogin() {
            if(this.email=='' || this.password=='')
                return

            this.showLogin = false;
        }
    },
    mounted() {
        this.getUsers();
    }
}

const _app = Vue.createApp(app);