_app.component('menu-component', {
    template: `
    <div class="container">
    <div class="row">
        <form class="col s12 m12">
          <h3 class="center-align">Hola {{ this.email }}</h3>
          <div class="row">
            <div class="input-field col s12">
              <i class="material-icons prefix">search</i>
              <input id="icon_prefix" type="text" class="validate">
              <label for="icon_prefix">Busca productos y tiendas</label>
            </div>
          </div>
        </form>
    </div>

    <div class="row">
        <h5>Categorias</h5>
        <div v-for="categorie in categories" :key="categorie.id" class="col s12 m4 l3">
            <div class="card">
                <div class="card-image">
                  <img :src="categorie.picture" alt="" width="100" height="100">
                </div>
                <div class="card-content center-align">
                  <p>{{ categorie.name }}</p>
                </div>
              </div>
        </div>
    </div>

    <div class="row" id="content-stores">
        <h5 class="header"><i class="material-icons prefix">shopping_cart</i> Tiendas</h5>
        <div v-for="store in stores" :key="store.id" class="col s12 m6">
            <div class="card horizontal">
              <div class="card-image">
                <img :src="store.picture" alt="" width="100" height="80">
              </div>
              <div class="card-stacked">
                <div class="card-content">
                    <b>{{ store.name }}</b>
                    <p class="mt-6 grey-text text-darken-1 valign-wrapper"><i class="material-icons prefix">location_on</i> <span>{{ store.location }}</span> <i class="material-icons prefix" style="margin-left:8px;">star_border</i> <span>{{ store.rating }}</span></p>
                    <p class="mt-8 grey-text text-darken-3">
                        <span v-for="label in store.labels" class="fz-12">{{ label }}. </span>..
                    </p>
                </div>
              </div>
            </div>
        </div>
    </div>

    <div class="row" id="content-promos">
        <h5 class="header">% Promos</h5>
        <div v-for="promo in promos" :key="promo.id" class="col s12 m4 l3">
            <div class="card">
                <div class="card-image">
                  <img :src="promo.picture" alt="" width="100" height="100">
                  <a class="btn-floating halfway-fab waves-effect waves-light red" style="top:0;margin-top:10px;">$ {{ promo.price }}</a>
                </div>
                <div class="card-content">
                  <p><b>{{ promo.name }}</b></p>
                  <p class="mt-6 fz-11 grey-text text-darken-1 valign-wrapper"><i class="material-icons prefix">location_on</i> <span>{{ promo.store }}</span> <i class="material-icons prefix" style="margin-left:8px;">star_border</i> <span>{{ promo.rating }}</span></p>
                </div>
              </div>
        </div>
    </div>

</div>
    `,
    props: {
        email: String
    },
    data() {
        return{
            usuarios: [],
            categories: [],
            stores: [],
            promos: []
        }
    },
    methods: {
        getCategories() {
            fetch('https://969rgz78f9.execute-api.us-east-1.amazonaws.com/dev/api/categories')
            .then(response => response.json())
            .then(data => {
                this.categories = data;
            });
        },
        getStores() {
            fetch('https://969rgz78f9.execute-api.us-east-1.amazonaws.com/dev/api/stores')
            .then(response => response.json())
            .then(data => {
                this.stores = data;
                //console.log(this.stores[0].labels)
            });
        },
        getPromos() {
            fetch('https://969rgz78f9.execute-api.us-east-1.amazonaws.com/dev/api/promos')
            .then(response => response.json())
            .then(data => {
                this.promos = data;
                //console.log(this.promos[0].name)
            });
        }
    },
    mounted() {
        this.getCategories();
        this.getStores();
        this.getPromos();
    },
    computed: {
    }
});